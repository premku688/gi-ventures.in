<!DOCTYPE html>
<html lang="en">


<head>

    <!-- Metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="GI VENTURES">
    <meta name="description" content="GI VENTURES">
    <meta name="author" content="Logas.tech">

    <!-- Title  -->
    <title>GI VENTURES</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/imgs/favicon.ico">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&amp;display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Sora:wght@100;200;300;400;500;600;700;800&amp;display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&amp;display=swap"
        rel="stylesheet">

    <!-- Plugins -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Core Style Css -->
    <link rel="stylesheet" href="assets/css/style.css">

</head>

<body class="dots-waves">



    <!-- ==================== Start Loading ==================== -->

    <div class="loader-wrap">
        <svg viewBox="0 0 1000 1000" preserveAspectRatio="none">
            <path id="svg" d="M0,1005S175,995,500,995s500,5,500,5V0H0Z"></path>
        </svg>

        <div class="loader-wrap-heading">
            <div class="load-text">
                <span>G</span>
                <span>I</span>
                <span></span>
                <span>V</span>
                <span>E</span>
                <span>N</span>
                <span>T</span>
                <span>U</span>
                <span>R</span>
                <span>E</span>
                <span>S</span>
            </div>
        </div>
    </div>

    <!-- ==================== End Loading ==================== -->


    <div class="cursor"></div>


    <!-- ==================== Start progress-scroll-button ==================== -->

    <div class="progress-wrap cursor-pointer">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>

    <!-- ==================== End progress-scroll-button ==================== -->


    <div id="home_wave"></div>


    <!-- ==================== Start Navbar ==================== -->

    <nav class="navbar navbar-expand-lg static">
        <div class="container">

            <!-- Logo -->
            <a class="logo icon-img-180" href="#">
                <img src="assets/imgs/logo-light.png" alt="logo">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"><i class="fas fa-bars"></i></span>
            </button>

            <!-- navbar links -->
            <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#0" data-scroll-nav="0"><span class="rolling-text">Home</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#2" data-scroll-nav="2"><span
                                class="rolling-text">Investors</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#3" data-scroll-nav="3"><span class="rolling-text">Startups</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#4" data-scroll-nav="4"><span
                                class="rolling-text">Portfolio</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#5" data-scroll-nav="5"><span
                                class="rolling-text">Case Study</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#6" data-scroll-nav="6"><span class="rolling-text">Team</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#7" data-scroll-nav="7"><span class="rolling-text">Contact</span></a>
                    </li>
                </ul>
            </div>

            <!-- <div class="md-hide">
                <div class="butn-presv">
                    <a href="#0" class="butn butn-sm butn-bg bg-white radius-5 skew">
                        <span class="text-dark">Shedule a Meeting</span>
                    </a>
                </div>
            </div> -->
        </div>
    </nav>

    <!-- ==================== End Navbar ==================== -->


    <main>


        <!-- ==================== Start main box ==================== -->

        <section class="box">

            <div class="container">
                <div class="row md-marg">
                    <div class="col-lg-12">
                        <!-- ==================== Start about ==================== -->

                        <div class="about section-padding" data-scroll-index="0">
                            <div class="cont text-center">
                                <img src="./assets/imgs/header/bg.png" style="width:50%!important" alt="">
                            </div>
                            <div class="stauts mt-80">
                                <div class="d-flex align-items-center  justify-content-center">
                                    <div class="mr-40">
                                        <div class="d-flex align-items-center">
                                            <h2>14</h2>
                                            <p>Years <br> of Experance</p>
                                        </div>
                                    </div>
                                    <div class="mr-40">
                                        <div class="d-flex align-items-center">
                                            <h2>6k</h2>
                                            <p>Clients <br> Worldwide</p>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="butn-presv">
                                            <a href="#0" class="butn butn-md butn-bord radius-5 skew">
                                                <span>Dwonload Brochure</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- ==================== End about ==================== -->



                        <!-- ==================== Start Resume ==================== -->

                        <div class="resume section-padding pt-0" data-scroll-index="1">
                            <div class="text mt-80">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sec-head bord-thin-bottom pb-20 mb-80">
                                            <h4 class="sub-title fz-28"><b>ABOUT US</b> <a href="#0"><img
                                                        src="assets/imgs/prod/arrow.png" class="arrow-img" alt=""></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <p class="fz-16"><b>GI Ventures</b> is a dynamic and innovative investment firm that
                                    operates at the <b>intersection of
                                        Venture Capital, Syndicate Collaborations, and Investment Banking.</b> With a
                                    strong focus on supporting
                                    early-stage and growth companies, GI Ventures specialises in providing <b>Financial
                                        Resources,
                                        Strategic Guidance, and Networking opportunities</b> to fuel entrepreneurial
                                    success.</p>

                                <p class="fz-16 my-3">At the core of GI Ventures’s approach, is the concept of syndicate
                                    formation. The company brings
                                    together a diverse group of investors, including <b>Venture Capital firms, Corporate
                                        Investment Arms,
                                        Family Offices, Super Angels, and Seed Angels,</b> to collaboratively invest in
                                    promising ventures. By
                                    pooling their resources and expertise, the syndicate members share the risks and
                                    rewards associated
                                    with these investments, enabling them to participate in opportunities that would be
                                    challenging to
                                    pursue individually.</p>
                                <p class="fz-16 my-3">We stand out as a forward-thinking investment firm, that combines
                                    the strengths of
                                    <b>Syndicate Collaborations, Investment Banking Expertise and
                                        Acceleration/Networking Support.</b> By
                                    bridging the gap between investors and entrepreneurs, the company plays a vital role
                                    in fueling the
                                    growth of innovative start-ups and facilitating their journey towards achieving
                                    remarkable milestones in
                                    the dynamic business landscape.
                                </p>
                            </div>
                            <div class="swiper5 mt-80" data-carousel="swiper" data-items="5" data-loop="true"
                                data-space="40" data-speed="1000">
                                <div id="content-carousel-container-unq-clients" class="swiper-container"
                                    data-swiper="container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="item">
                                                <div class="img icon-img-140">
                                                    <a href="#0"><img src="assets/imgs/prod/01.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item">
                                                <div class="img icon-img-140">
                                                    <a href="#0"><img src="assets/imgs/prod/02.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item">
                                                <div class="img icon-img-140">
                                                    <a href="#0"><img src="assets/imgs/prod/03.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item">
                                                <div class="img icon-img-140">
                                                    <a href="#0"><img src="assets/imgs/prod/04.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item">
                                                <div class="img icon-img-140">
                                                    <a href="#0"><img src="assets/imgs/prod/05.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- ==================== End Resume ==================== -->


                    </div>
                </div>
            </div>

        </section>

        <!-- ==================== End main box ==================== -->



        <!-- ==================== Start Services ==================== -->

        <section class="services section-padding pt-40" data-scroll-index="2">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sec-head bord-thin-bottom pb-20 mb-80">
                            <h4 class="sub-title fz-28">FOR INVESTORS</h4>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-6">
                        <div class="item md-mb30">
                            <img src="assets/imgs/investors/01.png" class="icon-img-90" alt="">
                            <h3 class="light-blue mt-2">01</h3>
                            <h6 class="fz-16">Curated Opportunities</h6>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="item md-mb30">
                            <img src="assets/imgs/investors/02.png" class="icon-img-90" alt="">
                            <h3 class="light-blue mt-2">02</h3>
                            <h6 class="fz-16">Handholding for Investement & Exit</h6>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="item sm-mb30">
                            <img src="assets/imgs/investors/03.png" class="icon-img-90" alt="">
                            <h3 class="light-blue mt-2">03</h3>
                            <h6 class="fz-16">Transaction, Structuring
                                & Paperwork Support</h6>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="item">
                            <img src="assets/imgs/investors/04.png" class="icon-img-90" alt="">
                            <h3 class="light-blue mt-2">04</h3>
                            <h6 class="fz-16">Financial Evaluation</h6>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 my-3">
                        <div class="item md-mb30">
                            <img src="assets/imgs/investors/05.png" class="icon-img-90" alt="">
                            <h3 class="light-blue mt-2">05</h3>
                            <h6 class="fz-16">Investment Banking</h6>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 my-3">
                        <div class="item sm-mb30">
                            <img src="assets/imgs/investors/06.png" class="icon-img-90" alt="">
                            <h3 class="light-blue mt-2">06</h3>
                            <h6 class="fz-16">Family office Advisory</h6>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 my-3">
                        <div class="item">
                            <img src="assets/imgs/investors/07.png" class="icon-img-90" alt="">
                            <h3 class="light-blue mt-2">07</h3>
                            <h6 class="fz-16">Alternate Investment Fund</h6>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ==================== End Services ==================== -->



        <!-- ==================== Start Portfolio ==================== -->

        <section class="portfolio section-padding pt-0" data-scroll-index="3">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sec-head bord-thin-bottom pb-20 mb-30">
                            <h4 class="sub-title fz-28">FOR STARTUPS</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="mt-50">
                            <p><b>GI Ventures</b> provides solutions for startups
                                which include Funding, Acceleration,
                                Investment Banking Support and
                                Networking opportunities
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="">
                            <img src="assets/imgs/investors/09.png" width="100%" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ==================== End Portfolio ==================== -->



        <!-- ==================== Start Testimonials ==================== -->

        <section class="testim-crv" data-scroll-index="4">
            <div class="container">
                <div class="sec-head bord-thin-bottom pb-20 mb-80">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="sub-title fz-28">OUR PORTFOLIO</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <img src="assets/imgs/prod/GI_VENTURES.png" alt="">
                    </div>
                </div>
            </div>
        </section>

        <!-- ==================== End Testimonials ==================== -->



        <!-- ==================== Start Blog ==================== -->

        <section class="blog section-padding" data-scroll-index="5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sec-head bord-thin-bottom pb-20 mb-80">
                            <h4 class="sub-title fz-28">INVESTMENT CASE STUDIES - 2022</h4>
                        </div>
                    </div>
                </div>
                <div class="row md-marg">
                    <div class="col-lg-6">
                        <div class="item md-mb30">
                            <div class="img">
                                <img src="assets/imgs/case-study/01.png" alt="">
                            </div>
                            <div class="box">
                                <div class="cont">
                                    <span class="date"><i class="fas fa-calendar-alt mr-10 main-color"></i> 6 , Aug
                                        2022</span>
                                    <h5><a href="#">PickMyWork To Empower 1 Lakh Gig Worker With Livelihood</a></h5>
                                </div>
                                <div class="info d-flex align-items-center">
                                    <div>
                                        <span><i class="fas fa-comments fz-12 mr-5"></i> 2 Comments</span>
                                    </div>
                                    <div class="ml-auto">
                                        <a href="#">Read More <svg class="ml-5" width="18" height="18"
                                                viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M17.2031 10.3281L11.5781 15.9531C11.535 15.9961 11.4839 16.0303 11.4276 16.0536C11.3713 16.077 11.3109 16.089 11.25 16.089C11.1891 16.089 11.1287 16.077 11.0724 16.0536C11.0161 16.0303 10.965 15.9961 10.9219 15.9531C10.8788 15.91 10.8446 15.8588 10.8213 15.8025C10.798 15.7462 10.786 15.6859 10.786 15.6249C10.786 15.564 10.798 15.5036 10.8213 15.4473C10.8446 15.391 10.8788 15.3399 10.9219 15.2968L15.7422 10.4687H3.125C3.00068 10.4687 2.88145 10.4193 2.79354 10.3314C2.70564 10.2435 2.65625 10.1242 2.65625 9.99993C2.65625 9.87561 2.70564 9.75638 2.79354 9.66847C2.88145 9.58056 3.00068 9.53118 3.125 9.53118H15.7422L10.9219 4.70305C10.8349 4.61603 10.786 4.498 10.786 4.37493C10.786 4.25186 10.8349 4.13383 10.9219 4.0468C11.0089 3.95978 11.1269 3.91089 11.25 3.91089C11.3731 3.91089 11.4911 3.95978 11.5781 4.0468L17.2031 9.6718C17.2476 9.71412 17.2829 9.76503 17.3071 9.82143C17.3313 9.87784 17.3438 9.93856 17.3438 9.99993C17.3438 10.0613 17.3313 10.122 17.3071 10.1784C17.2829 10.2348 17.2476 10.2857 17.2031 10.3281Z"
                                                    fill="currentColor"></path>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item md-mb30">
                            <div class="img">
                                <img src="assets/imgs/case-study/02.png" alt="">
                            </div>
                            <div class="box">
                                <div class="cont">
                                    <span class="date"><i class="fas fa-calendar-alt mr-10 main-color"></i> 6 , Aug
                                        2022</span>
                                    <h5><a href="#">PickMyWork Raises $1 Million In Seed Funding</a></h5>
                                </div>
                                <div class="info d-flex align-items-center">
                                    <div>
                                        <span><i class="fas fa-comments fz-12 mr-5"></i> 2 Comments</span>
                                    </div>
                                    <div class="ml-auto">
                                        <a href="#">Read More <svg class="ml-5" width="18" height="18"
                                                viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M17.2031 10.3281L11.5781 15.9531C11.535 15.9961 11.4839 16.0303 11.4276 16.0536C11.3713 16.077 11.3109 16.089 11.25 16.089C11.1891 16.089 11.1287 16.077 11.0724 16.0536C11.0161 16.0303 10.965 15.9961 10.9219 15.9531C10.8788 15.91 10.8446 15.8588 10.8213 15.8025C10.798 15.7462 10.786 15.6859 10.786 15.6249C10.786 15.564 10.798 15.5036 10.8213 15.4473C10.8446 15.391 10.8788 15.3399 10.9219 15.2968L15.7422 10.4687H3.125C3.00068 10.4687 2.88145 10.4193 2.79354 10.3314C2.70564 10.2435 2.65625 10.1242 2.65625 9.99993C2.65625 9.87561 2.70564 9.75638 2.79354 9.66847C2.88145 9.58056 3.00068 9.53118 3.125 9.53118H15.7422L10.9219 4.70305C10.8349 4.61603 10.786 4.498 10.786 4.37493C10.786 4.25186 10.8349 4.13383 10.9219 4.0468C11.0089 3.95978 11.1269 3.91089 11.25 3.91089C11.3731 3.91089 11.4911 3.95978 11.5781 4.0468L17.2031 9.6718C17.2476 9.71412 17.2829 9.76503 17.3071 9.82143C17.3313 9.87784 17.3438 9.93856 17.3438 9.99993C17.3438 10.0613 17.3313 10.122 17.3071 10.1784C17.2829 10.2348 17.2476 10.2857 17.2031 10.3281Z"
                                                    fill="currentColor"></path>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog mb-100" data-scroll-index="5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sec-head bord-thin-bottom pb-20 mb-80">
                            <h4 class="sub-title fz-28">INVESTMENT CASE STUDIES - 2021</h4>
                        </div>
                    </div>
                </div>
                <div class="row md-marg">
                    <div class="col-lg-6">
                        <div class="item md-mb30">
                            <div class="img">
                                <img src="assets/imgs/case-study/03.png" alt="">
                            </div>
                            <div class="box">
                                <div class="cont">
                                    <span class="date"><i class="fas fa-calendar-alt mr-10 main-color"></i> 6 , Aug
                                        2022</span>
                                    <h5><a href="#">[FUNDING ALERT] FINTECH STARTUP YPAY RAISES $400K LED BY WE FOUNDER CIRCLE</a></h5>
                                </div>
                                <div class="info d-flex align-items-center">
                                    <div>
                                        <span><i class="fas fa-comments fz-12 mr-5"></i> 2 Comments</span>
                                    </div>
                                    <div class="ml-auto">
                                        <a href="#">Read More <svg class="ml-5" width="18" height="18"
                                                viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M17.2031 10.3281L11.5781 15.9531C11.535 15.9961 11.4839 16.0303 11.4276 16.0536C11.3713 16.077 11.3109 16.089 11.25 16.089C11.1891 16.089 11.1287 16.077 11.0724 16.0536C11.0161 16.0303 10.965 15.9961 10.9219 15.9531C10.8788 15.91 10.8446 15.8588 10.8213 15.8025C10.798 15.7462 10.786 15.6859 10.786 15.6249C10.786 15.564 10.798 15.5036 10.8213 15.4473C10.8446 15.391 10.8788 15.3399 10.9219 15.2968L15.7422 10.4687H3.125C3.00068 10.4687 2.88145 10.4193 2.79354 10.3314C2.70564 10.2435 2.65625 10.1242 2.65625 9.99993C2.65625 9.87561 2.70564 9.75638 2.79354 9.66847C2.88145 9.58056 3.00068 9.53118 3.125 9.53118H15.7422L10.9219 4.70305C10.8349 4.61603 10.786 4.498 10.786 4.37493C10.786 4.25186 10.8349 4.13383 10.9219 4.0468C11.0089 3.95978 11.1269 3.91089 11.25 3.91089C11.3731 3.91089 11.4911 3.95978 11.5781 4.0468L17.2031 9.6718C17.2476 9.71412 17.2829 9.76503 17.3071 9.82143C17.3313 9.87784 17.3438 9.93856 17.3438 9.99993C17.3438 10.0613 17.3313 10.122 17.3071 10.1784C17.2829 10.2348 17.2476 10.2857 17.2031 10.3281Z"
                                                    fill="currentColor"></path>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item md-mb30">
                            <div class="img">
                                <img src="assets/imgs/case-study/04.png" alt="">
                            </div>
                            <div class="box">
                                <div class="cont">
                                    <span class="date"><i class="fas fa-calendar-alt mr-10 main-color"></i> 6 , Aug
                                        2022</span>
                                    <h5><a href="#">Fintech Startup YPay Raises $400,000 Bridge Round From We Founder Circle</a></h5>
                                </div>
                                <div class="info d-flex align-items-center">
                                    <div>
                                        <span><i class="fas fa-comments fz-12 mr-5"></i> 2 Comments</span>
                                    </div>
                                    <div class="ml-auto">
                                        <a href="#">Read More <svg class="ml-5" width="18" height="18"
                                                viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M17.2031 10.3281L11.5781 15.9531C11.535 15.9961 11.4839 16.0303 11.4276 16.0536C11.3713 16.077 11.3109 16.089 11.25 16.089C11.1891 16.089 11.1287 16.077 11.0724 16.0536C11.0161 16.0303 10.965 15.9961 10.9219 15.9531C10.8788 15.91 10.8446 15.8588 10.8213 15.8025C10.798 15.7462 10.786 15.6859 10.786 15.6249C10.786 15.564 10.798 15.5036 10.8213 15.4473C10.8446 15.391 10.8788 15.3399 10.9219 15.2968L15.7422 10.4687H3.125C3.00068 10.4687 2.88145 10.4193 2.79354 10.3314C2.70564 10.2435 2.65625 10.1242 2.65625 9.99993C2.65625 9.87561 2.70564 9.75638 2.79354 9.66847C2.88145 9.58056 3.00068 9.53118 3.125 9.53118H15.7422L10.9219 4.70305C10.8349 4.61603 10.786 4.498 10.786 4.37493C10.786 4.25186 10.8349 4.13383 10.9219 4.0468C11.0089 3.95978 11.1269 3.91089 11.25 3.91089C11.3731 3.91089 11.4911 3.95978 11.5781 4.0468L17.2031 9.6718C17.2476 9.71412 17.2829 9.76503 17.3071 9.82143C17.3313 9.87784 17.3438 9.93856 17.3438 9.99993C17.3438 10.0613 17.3313 10.122 17.3071 10.1784C17.2829 10.2348 17.2476 10.2857 17.2031 10.3281Z"
                                                    fill="currentColor"></path>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog" data-scroll-index="5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sec-head bord-thin-bottom pb-20 mb-80">
                            <h4 class="sub-title fz-28">INVESTMENT CASE STUDIES - 2020</h4>
                        </div>
                    </div>
                </div>
                <div class="row md-marg">
                    <div class="col-lg-6">
                        <div class="item md-mb30">
                            <div class="img">
                                <img src="assets/imgs/case-study/05.png" alt="">
                            </div>
                            <div class="box">
                                <div class="cont">
                                    <span class="date"><i class="fas fa-calendar-alt mr-10 main-color"></i> 6 , Aug
                                        2022</span>
                                    <h5><a href="#">Knocksense Raising $1 Mn In Pre-Series A Funding, Gets Nazara Founder's Backing</a></h5>
                                </div>
                                <div class="info d-flex align-items-center">
                                    <div>
                                        <span><i class="fas fa-comments fz-12 mr-5"></i> 2 Comments</span>
                                    </div>
                                    <div class="ml-auto">
                                        <a href="#">Read More <svg class="ml-5" width="18" height="18"
                                                viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M17.2031 10.3281L11.5781 15.9531C11.535 15.9961 11.4839 16.0303 11.4276 16.0536C11.3713 16.077 11.3109 16.089 11.25 16.089C11.1891 16.089 11.1287 16.077 11.0724 16.0536C11.0161 16.0303 10.965 15.9961 10.9219 15.9531C10.8788 15.91 10.8446 15.8588 10.8213 15.8025C10.798 15.7462 10.786 15.6859 10.786 15.6249C10.786 15.564 10.798 15.5036 10.8213 15.4473C10.8446 15.391 10.8788 15.3399 10.9219 15.2968L15.7422 10.4687H3.125C3.00068 10.4687 2.88145 10.4193 2.79354 10.3314C2.70564 10.2435 2.65625 10.1242 2.65625 9.99993C2.65625 9.87561 2.70564 9.75638 2.79354 9.66847C2.88145 9.58056 3.00068 9.53118 3.125 9.53118H15.7422L10.9219 4.70305C10.8349 4.61603 10.786 4.498 10.786 4.37493C10.786 4.25186 10.8349 4.13383 10.9219 4.0468C11.0089 3.95978 11.1269 3.91089 11.25 3.91089C11.3731 3.91089 11.4911 3.95978 11.5781 4.0468L17.2031 9.6718C17.2476 9.71412 17.2829 9.76503 17.3071 9.82143C17.3313 9.87784 17.3438 9.93856 17.3438 9.99993C17.3438 10.0613 17.3313 10.122 17.3071 10.1784C17.2829 10.2348 17.2476 10.2857 17.2031 10.3281Z"
                                                    fill="currentColor"></path>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item md-mb30">
                            <div class="img">
                                <img src="assets/imgs/case-study/06.png" alt="">
                            </div>
                            <div class="box">
                                <div class="cont">
                                    <span class="date"><i class="fas fa-calendar-alt mr-10 main-color"></i> 6 , Aug
                                        2022</span>
                                    <h5><a href="#">[FUNDING ALERT] HYPERLOCAL CONTENT PLATFORM KNOCKSENSE RAISES $150K IN BRIDGE ROUND ...</a></h5>
                                </div>
                                <div class="info d-flex align-items-center">
                                    <div>
                                        <span><i class="fas fa-comments fz-12 mr-5"></i> 2 Comments</span>
                                    </div>
                                    <div class="ml-auto">
                                        <a href="#">Read More <svg class="ml-5" width="18" height="18"
                                                viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M17.2031 10.3281L11.5781 15.9531C11.535 15.9961 11.4839 16.0303 11.4276 16.0536C11.3713 16.077 11.3109 16.089 11.25 16.089C11.1891 16.089 11.1287 16.077 11.0724 16.0536C11.0161 16.0303 10.965 15.9961 10.9219 15.9531C10.8788 15.91 10.8446 15.8588 10.8213 15.8025C10.798 15.7462 10.786 15.6859 10.786 15.6249C10.786 15.564 10.798 15.5036 10.8213 15.4473C10.8446 15.391 10.8788 15.3399 10.9219 15.2968L15.7422 10.4687H3.125C3.00068 10.4687 2.88145 10.4193 2.79354 10.3314C2.70564 10.2435 2.65625 10.1242 2.65625 9.99993C2.65625 9.87561 2.70564 9.75638 2.79354 9.66847C2.88145 9.58056 3.00068 9.53118 3.125 9.53118H15.7422L10.9219 4.70305C10.8349 4.61603 10.786 4.498 10.786 4.37493C10.786 4.25186 10.8349 4.13383 10.9219 4.0468C11.0089 3.95978 11.1269 3.91089 11.25 3.91089C11.3731 3.91089 11.4911 3.95978 11.5781 4.0468L17.2031 9.6718C17.2476 9.71412 17.2829 9.76503 17.3071 9.82143C17.3313 9.87784 17.3438 9.93856 17.3438 9.99993C17.3438 10.0613 17.3313 10.122 17.3071 10.1784C17.2829 10.2348 17.2476 10.2857 17.2031 10.3281Z"
                                                    fill="currentColor"></path>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ==================== End Blog ==================== -->

        <section class="testim-crv section-padding" data-scroll-index="6">
            <div class="container">
                <div class="sec-head bord-thin-bottom pb-20 mb-80">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="sub-title fz-28">OUR CORE TEAM</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <img src="assets/imgs/prod/PARTNERS.png" alt="">
                    </div>
                </div>
            </div>
        </section>

        <!-- ==================== Start Contact ==================== -->

        <section class="contact section-padding bord-thin-top" data-scroll-index="7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="sec-head md-mb80">
                            <h6 class="dot-titl mb-10">Get In Touch</h6>
                            <h2 class="fz-50">Let's make your deals brilliant!</h2>
                            <p class="fz-15 mt-10">If you would like to work with us or just want to get in
                                touch, we’d love to hear from you!</p>
                            <div class="phone fz-30 fw-600 mt-30 underline">
                                <a href="#0" class="main-color">+1 840 841 25 69</a>
                            </div>
                            <ul class="rest social-text d-flex mt-60">
                                <li class="mr-30">
                                    <a href="#0" class="hover-this"><span class="hover-anim">Facebook</span></a>
                                </li>
                                <li class="mr-30">
                                    <a href="#0" class="hover-this"><span class="hover-anim">Twitter</span></a>
                                </li>
                                <li class="mr-30">
                                    <a href="#0" class="hover-this"><span class="hover-anim">LinkedIn</span></a>
                                </li>
                                <li>
                                    <a href="#0" class="hover-this"><span class="hover-anim">Instagram</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1 valign">
                        <div class="full-width">
                            <form id="contact-form" method="post"
                                action="https://ui-themez.smartinnovates.net/items/hawke/contact.php">

                                <div class="messages"></div>

                                <div class="controls row">

                                    <div class="col-lg-6">
                                        <div class="form-group mb-30">
                                            <input id="form_name" type="text" name="name" placeholder="Name"
                                                required="required">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group mb-30">
                                            <input id="form_email" type="email" name="email" placeholder="Email"
                                                required="required">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group mb-30">
                                            <input id="form_subject" type="text" name="subject" placeholder="Subject">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea id="form_message" name="message" placeholder="Message" rows="4"
                                                required="required"></textarea>
                                        </div>
                                        <div class="mt-30">
                                            <button type="submit">
                                                <span class="text">Send A Message</span>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ==================== End Contact ==================== -->


    </main>


    <!-- ==================== Start Footer ==================== -->

    <footer class="pt-30 pb-30">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        <p class="fz-13">© 2023 GI VENTURES</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- ==================== End Footer ==================== -->










    <!-- jQuery -->
    <script src="assets/js/jquery-3.6.0.min.js"></script>
    <script src="assets/js/jquery-migrate-3.4.0.min.js"></script>

    <!-- plugins -->
    <script src="assets/js/plugins.js"></script>

    <script src="assets/js/ScrollTrigger.min.js"></script>

    <!-- custom scripts -->
    <script src="assets/js/scripts.js"></script>

    <script src="assets/js/three.min.js"></script>

    <script src="assets/js/cdemo.js"></script>

</body>


<!-- Mirrored from ui-themez.smartinnovates.net/items/hawke/home-dots-waves.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 31 May 2023 14:39:53 GMT -->

</html>